syntax on
filetype plugin indent on

set expandtab
set smarttab
set shiftwidth=2
set tabstop=2
set ai
set si

set number
set laststatus=2

set t_Co=256
set background=dark

autocmd BufNewFile,BufRead *.md set filetype=markdown
