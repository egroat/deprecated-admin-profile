# ~/.bashrc: executed by bash(1) for non-login shells.

# * * * * * * * * * * * * * * * #
#          Eoin Groat           #
#     Preferred Bash Setup      #
#      v1.5 Admin Edition       #
# * * * * * * * * * * * * * * * #


##
# Interactive only
##
case $- in
    *i*) ;;
      *) return;;
esac

# History
HISTCONTROL=ignoreboth  # duplicate, and lines starting with space
shopt -s histappend     # Append to file. Don't overwrite
HISTSIZE=1000
HISTFILESIZE=2000

# keep $LINES and $COLUMNS updated (after every command).
shopt -s checkwinsize

# "**" matches all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"


# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias clear='export first_command="" && clear'
    alias workshare='. workshare'
fi

# enable programmable completion features
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

##
# Important exports
##
export TZ="/usr/share/zoneinfo/GB"
export VISUAL="vim"


if [ -n "$HOME" ]; then
  ##
  # PATH
  ##
  if [ ! -e "$HOME/bin" ]; then
    mkdir "$HOME/bin"
  fi
  PATH="/usr/lib:$HOME/bin:$PATH"
fi

##
# Prompt
##
PS1=""
make_colour_prompt() {
  local red='\033[0;31m'
  local boldred='\033[1;31m'
  local green='\033[0;32m'
  local boldgreen='\033[1;32m'
  local yellow='\033[0;33m'
  local boldyellow='\033[1;33m'
  local blue='\033[0;34m'
  local boldblue='\033[1;34m'
  local purple='\033[0;35m'
  local boldpurple='\033[1;35m'
  local cyan='\033[0;36m'
  local boldcyan='\033[1;36m'
  local white='\033[0;37m'
  local boldwhite='\033[1;37m'
  local off='\033[0;00m'
  
  local br="$boldwhite|"
  local smiley="$boldpurple(${purple}*${boldpurple}_${purple}^$boldpurple)"
  local end="$off\n"

  local dir="$boldblue\w"
  local shell="$SHLVL"
  local result="$red\$?"
  local userhost="$boldred$USER$red@${HOSTNAME%%.*}"

  PS1="$result$br$userhost$br$dir $smiley$end"
}
make_colour_prompt

##
# Aliases
##
alias ll='ls -alFh' # List all, long, classify,, human readable
alias la='ls -A'    # List all
alias l='ls -CF'    # List columns and classify

alias gdb='gdb -q'  # Start GDB without a wall of text

if hash vim 2>/dev/null; then
  alias vi='vim'      # Avoid inflicting vi on oneself when possible
fi

##
# tmux
##
alias tmux='tmux -2' # tmux starts with 256 colours
#`export TERM=screen-256color

if [[ -z "$TMUX" && -n "$(which tmux 2>/dev/null)" ]]; then
  tmux && exit
fi
